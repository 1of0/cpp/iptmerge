#!/bin/bash

set -xe

package=iptmerge
binary_name=iptmerge
binary_source=cmake-build/bin/iptmerge

if ! git describe --tags &> /dev/null; then
    VERSION=0.0.1-1
else
    VERSION=$(git describe --tags)
fi

deb_base_dir="deb/${package}_${VERSION}-${BUILD_ARCH:-amd64}"

rm -Rf "deb/"
mkdir -p "${deb_base_dir}/usr/bin";
mkdir -p "${deb_base_dir}/usr/share/doc/${package}";
mkdir -p "${deb_base_dir}/usr/share/lintian/overrides";

cp -Rv ".ci/dist/DEBIAN" "${deb_base_dir}/"

cp -v ".ci/dist/copyright" "${deb_base_dir}/usr/share/doc/${package}/copyright"
cp -v ".ci/dist/changelog" "${deb_base_dir}/usr/share/doc/${package}/changelog.Debian"
cp -v ".ci/dist/lintian-overrides" "${deb_base_dir}/usr/share/lintian/overrides/${package}"

sed -i "s/@version@/${VERSION}/g" "${deb_base_dir}/DEBIAN/control"
sed -i "s/@arch@/${BUILD_ARCH:-amd64}/g" "${deb_base_dir}/DEBIAN/control"

sed -i "s/@version@/${VERSION}/g" "${deb_base_dir}/usr/share/doc/${package}/changelog.Debian"
sed -i "s/@date@/$(date -R)/g"    "${deb_base_dir}/usr/share/doc/${package}/changelog.Debian"
gzip -n --best "${deb_base_dir}/usr/share/doc/${package}/changelog.Debian"

${BUILD_TOOL_PREFIX}strip --strip-debug --strip-unneeded -o "${deb_base_dir}/usr/bin/${binary_name}" "${binary_source}"

chown 0:0 -R "${deb_base_dir}"
chmod 0755 -R "${deb_base_dir}"
chmod 0644 ${deb_base_dir}/usr/share/doc/${package}/*
chmod 0644 ${deb_base_dir}/usr/share/lintian/overrides/*

dpkg-deb --build ${deb_base_dir}
