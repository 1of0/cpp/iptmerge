#!/bin/bash

[[ ! -e /.dockerenv ]] && exit 0

set -xe

DEBIAN_FRONTEND=noninteractive sudo apt update -yqq
DEBIAN_FRONTEND=noninteractive sudo apt install cmake valgrind gcovr -yqq

mkdir cmake-build
mkdir -p .conan/.conan/profiles

custom_conan_profile=".ci/conan-profile-${BUILD_ARCH}"

if [[ -f "${custom_conan_profile}" ]]; then
    cp -fv "${custom_conan_profile}" .conan/.conan/profiles/default
fi
