#!/bin/bash

[[ ! -e /.dockerenv ]] && exit 0

set -xe

DEBIAN_FRONTEND=noninteractive sudo apt update -yqq
DEBIAN_FRONTEND=noninteractive sudo apt install fakeroot lintian -yqq
