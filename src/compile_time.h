#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

// Provides a custom fmt formatter.
// Should not be included by other header files.

#include <fmt/format.h>
#include <type_traits>
#include <yaml-cpp/yaml.h>

#include "exceptions.h"
#include "merge/merge_behaviour.h"
#include "netfilter/structures.h"

namespace fmt
{
    template<typename T>
    struct formatter<T, enable_if_t<std::is_base_of<iptmerge::netfilter::structure::Object, T>::value, char>>
        : formatter<std::string>
    {
        template<typename FormatCtx>
        auto format(const iptmerge::netfilter::structure::Object& object, FormatCtx& context)
        {
            return formatter<std::string>::format(object.toString(), context);
        }
    };
}

namespace YAML
{
    template<>
    struct convert<iptmerge::merge::configuration::Unmanaged> {
        static Node encode(const iptmerge::merge::configuration::Unmanaged& instance)
        {
            throw std::runtime_error("Not implemented");
        }

        static bool decode(const Node& node, iptmerge::merge::configuration::Unmanaged& instance)
        {
            using std::string;
            using std::vector;
            using namespace iptmerge::merge::configuration;

            if (!node.IsMap()) {
                throw iptmerge::InvalidConfigurationException("Failed decoding 'unmanaged' node");
            }

            instance.chainStrategy = mapStringToEnum(
                node["chainStrategy"].as<string>("keep"),
                StringToStrategyKindMapping
            );

            instance.ruleStrategy = mapStringToEnum(
                node["ruleStrategy"].as<string>("keep"),
                StringToStrategyKindMapping
            );

            instance.ruleMatchAgainst = mapStringToEnum(
                node["ruleMatchAgainst"].as<string>("comment"),
                StringToMatchKindMapping
            );

            for (const string& whitelistPattern : node["chainWhitelists"].as<vector<string>>(vector<string>())) {
                instance.chainWhitelists.emplace_back(whitelistPattern);
            }

            for (const string& whitelistPattern : node["ruleWhitelists"].as<vector<string>>(vector<string>())) {
                instance.ruleWhitelists.emplace_back(whitelistPattern);
            }

            return true;
        }
    };

    template<>
    struct convert<std::regex> {
        static Node encode(const std::regex& instance)
        {
            throw std::runtime_error("Not implemented");
        }

        static bool decode(const Node& node, std::regex& instance)
        {
            if (!node.IsScalar()) {
                throw iptmerge::InvalidConfigurationException("Failed decoding regex");
            }
            instance.assign(node.as<std::string>());
            return true;
        }
    };

    template<>
    struct convert<iptmerge::merge::configuration::Managed> {
        static Node encode(const iptmerge::merge::configuration::Managed& instance)
        {
            throw std::runtime_error("Not implemented");
        }

        static bool decode(const Node& node, iptmerge::merge::configuration::Managed& instance)
        {
            using std::regex;

            if (!node.IsMap()) {
                throw iptmerge::InvalidConfigurationException("Failed decoding 'managed' node");
            }

            instance.chainPattern = node["chainPattern"].as<regex>(regex(""));
            instance.rulePattern = node["rulePattern"].as<regex>(regex(""));

            return true;
        }
    };

    template<>
    struct convert<iptmerge::merge::configuration::MergeBehaviour> {
        static Node encode(const iptmerge::merge::configuration::MergeBehaviour& instance)
        {
            throw std::runtime_error("Not implemented");
        }

        static bool decode(const Node& node, iptmerge::merge::configuration::MergeBehaviour& instance)
        {
            using iptmerge::merge::configuration::Managed;
            using iptmerge::merge::configuration::Unmanaged;

            if (!node.IsMap()) {
                throw iptmerge::InvalidConfigurationException("Failed decoding document");
            }

            instance.managed = node["managed"].as<Managed>(Managed{});
            instance.unmanaged = node["unmanaged"].as<Unmanaged>(Unmanaged{});

            return true;
        }
    };
}
