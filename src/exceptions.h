#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <exception>
#include <stdexcept>

namespace iptmerge
{
    class IptmergeException : public std::runtime_error
    {
    public:
        explicit IptmergeException(const std::string &arg) : runtime_error(arg) {}
    };

    class UnclosedStringException : public IptmergeException
    {
    public:
        explicit UnclosedStringException(const std::string &arg) : IptmergeException(arg) {}
    };

    class UnexpectedTokenException : public IptmergeException
    {
    public:
        explicit UnexpectedTokenException(const std::string &arg) : IptmergeException(arg) {}
    };

    class InvalidActionException : public IptmergeException
    {
    public:
        explicit InvalidActionException(const std::string &arg) : IptmergeException(arg) {}
    };

    class InvalidDefinitionException : public IptmergeException
    {
    public:
        explicit InvalidDefinitionException(const std::string &arg) : IptmergeException(arg) {}
    };

    class InvalidConfigurationException : public IptmergeException
    {
    public:
        explicit InvalidConfigurationException(const std::string &arg) : IptmergeException(arg) {}
    };
}
