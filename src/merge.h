#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include "merge/merge_behaviour.h"
#include "merge/state_merger.h"
#include "merge/strategies.h"
