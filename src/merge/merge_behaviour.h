#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <regex>
#include <string>
#include <unordered_map>
#include <vector>

#include "../exceptions.h"

namespace iptmerge::merge::configuration::detail
{
    using namespace std;

    inline namespace exports
    {
        enum StrategyKind
        {
            Keep,
            Whitelist,
            Purge,
        };

        enum MatchKind
        {
            Comment,
            Full,
        };

        static const unordered_map<string, StrategyKind> StringToStrategyKindMapping = {
            {"keep", StrategyKind::Keep},
            {"whitelist", StrategyKind::Whitelist},
            {"purge", StrategyKind::Purge},
        };

        static const unordered_map<string, MatchKind> StringToMatchKindMapping = {
            {"comment", MatchKind::Comment},
            {"full", MatchKind::Full},
        };

        template<typename T>
        static T mapStringToEnum(const string& input, unordered_map<string, T> mapping)
        {
            if (mapping.count(input) < 1) {
                throw InvalidConfigurationException(
                    "Cannot convert value to enum: " + input
                );
            }
            return mapping.at(input);
        }

        struct Managed
        {
            regex chainPattern;
            regex rulePattern;
        };

        struct Unmanaged
        {
            StrategyKind chainStrategy;
            StrategyKind ruleStrategy;
            MatchKind ruleMatchAgainst;
            vector<regex> chainWhitelists;
            vector<regex> ruleWhitelists;
        };

        struct MergeBehaviour
        {
            Managed managed;
            Unmanaged unmanaged;
        };
    }
}

namespace iptmerge::merge::configuration
{
    using namespace detail::exports;
}
