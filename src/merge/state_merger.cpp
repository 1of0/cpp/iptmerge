#include "state_merger.h"

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <memory>
#include <unordered_map>

namespace iptmerge::merge::detail
{
    using namespace iptmerge;
    using namespace std;

    const unordered_map<StrategyKind, shared_ptr<BaseStrategy>> ChainStrategyMapping = {
        {StrategyKind::Keep, make_shared<ChainKeep>()},
        {StrategyKind::Purge, make_shared<ChainPurge>()},
        {StrategyKind::Whitelist, make_shared<ChainWhitelist>()},
    };

    const unordered_map<StrategyKind, shared_ptr<BaseStrategy>> RuleStrategyMapping = {
        {StrategyKind::Keep, make_shared<RuleKeep>()},
        {StrategyKind::Purge, make_shared<RulePurge>()},
        {StrategyKind::Whitelist, make_shared<RuleWhitelist>()},
    };

    State StateMerger::merge(const State &base, const State &supplicant, const MergeBehaviour &mergeBehaviour)
    {
        auto baseTables = base.getTablesIndexedByName();
        auto supplicantTables = supplicant.getTablesIndexedByName();

        vector<Table> mergedTables;

        for (const auto& kv : baseTables) {
            string tableName = kv.first;
            Table baseTable = kv.second;
            Table supplicantTable = supplicantTables.count(tableName) > 0
                ? supplicantTables.at(tableName)
                : Table(tableName);

            supplicantTable = transplantCounters(baseTable, supplicantTable);
            supplicantTable = mergeChains(baseTable, supplicantTable, mergeBehaviour);
            supplicantTable = mergeRules(baseTable, supplicantTable, mergeBehaviour);
            supplicantTable = reapplyOriginalRuleOrder(supplicantTable);

            mergedTables.push_back(supplicantTable);
        }

        for (const auto& kv : supplicantTables) {
            string tableName = kv.first;
            Table supplicantTable = kv.second;

            if (baseTables.count(tableName) == 0) {
                mergedTables.push_back(supplicantTable);
            }
        }

        return State(mergedTables);
    }

    Table StateMerger::mergeChains(const Table &base, const Table &supplicant, const MergeBehaviour &mergeBehaviour)
    {
        return ChainStrategyMapping.at(mergeBehaviour.unmanaged.chainStrategy)->
            merge(base, supplicant, mergeBehaviour);
    }

    Table StateMerger::mergeRules(const Table &base, const Table &supplicant, const MergeBehaviour &mergeBehaviour)
    {
        return RuleStrategyMapping.at(mergeBehaviour.unmanaged.ruleStrategy)->
            merge(base, supplicant, mergeBehaviour);
    }

    Table StateMerger::reapplyOriginalRuleOrder(const Table &table)
    {
        return Table(
            table.getName(),
            table.getChains(),
            util::set::sortedBy<Rule>(
                table.getRules(),
                [](const Rule& a, const Rule& b) {
                    return (bool)(a.getIndex() < b.getIndex());
                }
            )
        );
    }

    Table StateMerger::transplantCounters(const Table &base, const Table &supplicant)
    {
        auto baseChains = base.getChainsIndexedByName();
        auto supplicantChains = supplicant.getChainsIndexedByName();

        for (auto& kv : supplicantChains) {
            string name = kv.first;
            Chain supplicantChain = kv.second;
            supplicantChains.at(name) = baseChains.count(name) > 0
                ? supplicantChain.withCounters(baseChains.at(name).getCounters())
                : supplicantChain;
        }

        return Table(
            supplicant.getName(),
            util::map::getValues(supplicantChains),
            supplicant.getRules()
        );
    }
}
