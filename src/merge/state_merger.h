#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include "strategies.h"
#include "../netfilter/structures.h"
#include "../util.h"

namespace iptmerge::merge::detail
{
    using namespace iptmerge::merge::configuration;
    using namespace iptmerge::merge::strategy;
    using namespace iptmerge::netfilter::structure;

    inline namespace exports
    {
        class StateMerger
        {
        private:
            [[nodiscard]] static Table mergeChains(
                const Table& base,
                const Table& supplicant,
                const MergeBehaviour& mergeBehaviour
            );

            [[nodiscard]] static Table mergeRules(
                const Table& base,
                const Table& supplicant,
                const MergeBehaviour& mergeBehaviour
            );

            [[nodiscard]] static Table transplantCounters(
                const Table& base,
                const Table& supplicant
            );

            [[nodiscard]] static Table reapplyOriginalRuleOrder(const Table &table);

        public:
            [[nodiscard]] static State merge(
                const State& base,
                const State& supplicant,
                const MergeBehaviour& mergeBehaviour
            );
        };
    }
}

namespace iptmerge::merge
{
    using namespace detail::exports;
}
