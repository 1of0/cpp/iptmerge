#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include "strategy/chain_keep.h"
#include "strategy/chain_purge.h"
#include "strategy/chain_whitelist.h"
#include "strategy/rule_keep.h"
#include "strategy/rule_purge.h"
#include "strategy/rule_whitelist.h"

namespace iptmerge::merge::strategy
{
    using namespace detail::exports;
}
