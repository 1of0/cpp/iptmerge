#include "base_strategy.h"

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <regex>

namespace iptmerge::merge::strategy::detail
{
    using namespace iptmerge;
    using namespace std;

    vector<Chain> BaseStrategy::getManagedChains(const vector<Chain>& chains, const MergeBehaviour& mergeBehaviour)
    {
        return util::set::where<Chain>(chains, [&mergeBehaviour](const Chain& chain) {
            return regex_search(chain.getName(), mergeBehaviour.managed.chainPattern);
        });
    }

    vector<Rule> BaseStrategy::getManagedRules(const vector<Rule>& rules, const MergeBehaviour& mergeBehaviour)
    {
        return util::set::where<Rule>(rules, [&mergeBehaviour](const Rule& rule) {
            optional<RuleOption> commentOption = rule.getFirstOption({"comment"});
            if (!commentOption.has_value() || commentOption->getValues().empty()) {
                return false;
            }
            return regex_search(commentOption->getValue(0).value(), mergeBehaviour.managed.rulePattern);
        });
    }

    vector<Chain> BaseStrategy::getWhitelistedChains(const vector<Chain>& chains, const MergeBehaviour& mergeBehaviour)
    {
        return util::set::where<Chain>(chains, [&mergeBehaviour](const Chain& chain) {
            for (const regex& pattern : mergeBehaviour.unmanaged.chainWhitelists) {
                if (regex_search(chain.getName(), pattern)) {
                    return true;
                }
            }
            return false;
        });
    }

    vector<Rule> BaseStrategy::getWhitelistedRules(const vector<Rule>& rules, const MergeBehaviour& mergeBehaviour)
    {
        return util::set::where<Rule>(rules, [&mergeBehaviour](const Rule& rule) {
            string matchAgainst;

            switch (mergeBehaviour.unmanaged.ruleMatchAgainst) {
                case configuration::Full:matchAgainst = rule.toString();
                    break;
                case configuration::Comment:optional<RuleOption> commentOption = rule.getFirstOption({"comment"});
                    if (!commentOption.has_value() || commentOption->getValues().empty()) {
                        return false;
                    }
                    matchAgainst = commentOption->getValue(0).value();
                    break;
            }

            for (const regex& pattern : mergeBehaviour.unmanaged.ruleWhitelists) {
                if (regex_search(matchAgainst, pattern)) {
                    return true;
                }
            }
            return false;
        });
    }

    vector<Rule> BaseStrategy::getChainDeletionRules(const vector<Chain>& chains)
    {
        return util::set::selectAs<Rule, Chain>(chains, [](const Chain& chain) {
            return Rule(-1, "X", chain.getName(), {});
        });
    }

    vector<Rule> BaseStrategy::getRuleDeletionRules(const vector<Rule>& rules)
    {
        return util::set::selectAs<Rule, Rule>(rules, [](const Rule& rule) {
            return rule.withAction("D").withIndex(-1);
        });
    }
}
