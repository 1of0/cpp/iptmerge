#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include "../merge_behaviour.h"
#include "../../netfilter/structures.h"
#include "../../util.h"

namespace iptmerge::merge::strategy::detail
{
    using namespace iptmerge::merge::configuration;
    using namespace iptmerge::netfilter::structure;
    using namespace std;

    inline namespace exports
    {
        class BaseStrategy
        {
        protected:
            static vector<Chain> getManagedChains(
                const vector<Chain>& chains,
                const MergeBehaviour& mergeBehaviour
            );

            static vector<Rule> getManagedRules(
                const vector<Rule>& rules,
                const MergeBehaviour& mergeBehaviour
            );

            static vector<Chain> getWhitelistedChains(
                const vector<Chain>& chains,
                const MergeBehaviour& mergeBehaviour
            );

            static vector<Rule> getWhitelistedRules(
                const vector<Rule>& rules,
                const MergeBehaviour& mergeBehaviour
            );

            static vector<Rule> getChainDeletionRules(const vector<Chain>& chains);

            static vector<Rule> getRuleDeletionRules(const vector<Rule>& rules);

        public:
            [[nodiscard]] virtual Table merge(
                const Table& base,
                const Table& supplicant,
                const MergeBehaviour& mergeBehaviour
            ) const = 0;
        };
    }
}
