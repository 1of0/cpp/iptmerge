#include "chain_keep.h"

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

namespace iptmerge::merge::strategy::detail
{
    using namespace iptmerge;

    Table ChainKeep::merge(
        const Table& base,
        const Table& supplicant,
        const MergeBehaviour& mergeBehaviour
    ) const {
        auto managedChains = getManagedChains(base.getChains(), mergeBehaviour);

        auto extraneousManagedChains = util::set::diffOf(managedChains, supplicant.getChains());

        auto baseChainsWithoutExtraneousChains = util::set::diffOf(base.getChains(), extraneousManagedChains);

        auto chainDeletions = getChainDeletionRules(extraneousManagedChains);

        return Table(
            supplicant.getName(),
            util::set::unionOf(supplicant.getChains(), baseChainsWithoutExtraneousChains),
            util::set::unionOf(chainDeletions, supplicant.getRules())
        );
    }
}
