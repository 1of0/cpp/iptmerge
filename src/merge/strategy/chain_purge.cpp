#include "chain_purge.h"

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

namespace iptmerge::merge::strategy::detail
{
    using namespace iptmerge;

    Table ChainPurge::merge(
        const Table& base,
        const Table& supplicant,
        const MergeBehaviour& mergeBehaviour
    ) const {
        auto extraneousManagedChains = util::set::diffOf(base.getChains(), supplicant.getChains());

        auto chainDeletions = getChainDeletionRules(extraneousManagedChains);

        return Table(
            supplicant.getName(),
            supplicant.getChains(),
            util::set::unionOf(chainDeletions, supplicant.getRules())
        );
    }
}
