#include "chain_whitelist.h"

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

namespace iptmerge::merge::strategy::detail
{
    using namespace iptmerge;

    Table ChainWhitelist::merge(
        const Table& base,
        const Table& supplicant,
        const MergeBehaviour& mergeBehaviour
    ) const {
        auto extraneousChains = util::set::diffOf(base.getChains(), supplicant.getChains());

        auto whitelisted = getWhitelistedChains(extraneousChains, mergeBehaviour);

        auto baseChainsToRemove = util::set::diffOf(extraneousChains, whitelisted);

        auto chainDeletions = getChainDeletionRules(baseChainsToRemove);

        return Table(
            supplicant.getName(),
            util::set::unionOf(supplicant.getChains(), whitelisted),
            util::set::unionOf(chainDeletions, supplicant.getRules())
        );
    }
}
