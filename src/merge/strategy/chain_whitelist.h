#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include "base_strategy.h"

namespace iptmerge::merge::strategy::detail
{
    inline namespace exports
    {
        class ChainWhitelist : public BaseStrategy
        {
        public:
            [[nodiscard]] Table merge(
                const Table& base,
                const Table& supplicant,
                const MergeBehaviour& mergeBehaviour
            ) const override;
        };
    }
}
