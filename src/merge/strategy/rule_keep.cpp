#include "rule_keep.h"

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

namespace iptmerge::merge::strategy::detail
{
    using namespace iptmerge;

    Table RuleKeep::merge(
        const Table& base,
        const Table& supplicant,
        const MergeBehaviour& mergeBehaviour
    ) const {
        auto managedRules = getManagedRules(base.getRules(), mergeBehaviour);

        auto extraneousManagedRules = util::set::diffOf(managedRules, supplicant.getRules());

        auto baseRulesWithoutExtraneousRules = util::set::diffOf(base.getRules(), extraneousManagedRules);

        auto ruleDeletions = getRuleDeletionRules(extraneousManagedRules);

        return Table(
            supplicant.getName(),
            supplicant.getChains(),
            util::set::unionOf(ruleDeletions, util::set::diffOf(supplicant.getRules(), baseRulesWithoutExtraneousRules))
        );
    }
}
