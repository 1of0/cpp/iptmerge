#include "rule_purge.h"

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

namespace iptmerge::merge::strategy::detail
{
    using namespace iptmerge;

    Table RulePurge::merge(
        const Table& base,
        const Table& supplicant,
        const MergeBehaviour& mergeBehaviour
    ) const {
        auto extraneousRules = util::set::diffOf(base.getRules(), supplicant.getRules());

        auto ruleDeletions = getRuleDeletionRules(extraneousRules);

        return Table(
            supplicant.getName(),
            supplicant.getChains(),
            util::set::unionOf(ruleDeletions, util::set::diffOf(supplicant.getRules(), base.getRules()))
        );
    }
}
