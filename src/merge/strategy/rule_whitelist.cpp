#include "rule_whitelist.h"

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

namespace iptmerge::merge::strategy::detail
{
    using namespace iptmerge;

    Table RuleWhitelist::merge(
        const Table& base,
        const Table& supplicant,
        const MergeBehaviour& mergeBehaviour
    ) const {
        auto extraneousRules = util::set::diffOf(base.getRules(), supplicant.getRules());

        auto whitelisted = getWhitelistedRules(extraneousRules, mergeBehaviour);

        auto baseRulesToRemove = util::set::diffOf(extraneousRules, whitelisted);

        auto ruleDeletions = getRuleDeletionRules(baseRulesToRemove);

        return Table(
            supplicant.getName(),
            supplicant.getChains(),
            util::set::unionOf(ruleDeletions, util::set::diffOf(supplicant.getRules(), base.getRules()))
        );
    }
}
