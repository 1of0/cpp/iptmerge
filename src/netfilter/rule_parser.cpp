#include "rule_parser.h"

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <fmt/format.h>
#include <Poco/Net/IPAddress.h>
#include <regex>
#include "rule_tokenizer.h"
#include "../exceptions.h"
#include "../util.h"

namespace iptmerge::netfilter::detail
{
    using namespace fmt;
    using namespace iptmerge;
    using namespace std;
    using namespace Poco::Net;

    unordered_map<string, vector<string>> OptionAliases = {
        { "A", {"append"}},
        { "I", {"insert"}},
        { "D", {"delete"}},
        { "N", {"new-chain"}},
        { "X", {"delete-chain"}},
        { "F", {"flush"}},
        { "s", {"src", "source"}},
        { "d", {"dst", "destination"}},
        { "m", {"match"}},
        { "p", {"protocol"}},
        { "j", {"jump"}},
        { "g", {"goto"}},
        { "i", {"in-interface"}},
        { "o", {"out-interface"}},
        { "f", {"fragment"}},
        { "c", {"set-counters"}},
        { "sport", {"source-port"}},
        { "sports", {"source-ports"}},
        { "dport", {"destination-port"}},
        { "dports", {"destination-ports"}},
    };

    vector<string> ChainActionOptions = {
        "A", "I", "D", "N", "X", "F"
    };

    vector<string> ExpandingOptions = {
        "s", "d"
    };

    vector<string> NonRepeatableOptions = {
        "s", "d", "comment"
    };

    vector<string> SingleValueOptions = {
        "A", "I", "D", "N", "X", "F", "s", "d", "comment"
    };

    vector<string> TcpSpecificMatchOptions = {
        "tcp-flags", "syn", "tcp-option"
    };

    vector<string> TcpMatchOptions = {
        "sport", "source-port", "dport", "destination-port", "tcp-flags", "syn", "tcp-option"
    };

    vector<string> UdpMatchOptions = {
        "sport", "source-port", "dport", "destination-port"
    };

    regex PatternCounter = regex(R"(^\[\d+:\d+\]$)");

    vector<Rule> RuleParser::parse(const string &ruleDefinition)
    {
        vector<RuleOption> parsedOptions;

        vector<vector<string>> groupedTokens = RuleTokenizer::groupTokens(RuleTokenizer::tokenize(ruleDefinition));

        string counters;

        for (vector<string> tokenGroup : groupedTokens) {
            bool negated = false;

            if (tokenGroup.empty()) {
                continue;
            }

            if (tokenGroup.size() == 1 && regex_match(tokenGroup[0], PatternCounter)) {
                counters = tokenGroup[0];
                continue;
            }

            if (tokenGroup[0] == RuleTokenizer::NegationToken) {
                negated = true;
                tokenGroup.erase(tokenGroup.begin());
            }

            if (tokenGroup.empty()) {
                throw UnexpectedTokenException(format("No option in token group: {}", join(tokenGroup, " ")));
            }

            string optionName = getOptionName(tokenGroup[0]);

            optional<string> foundCanonicalAlias = util::map::getKeyOfVectorContainingValue(OptionAliases, optionName);

            if (foundCanonicalAlias.has_value()) {
                optionName = foundCanonicalAlias.value();
            }

            tokenGroup.erase(tokenGroup.begin());

            parsedOptions.emplace_back(optionName, tokenGroup, negated);
        }

        PlainRuleOptionCollection options = PlainRuleOptionCollection(parsedOptions);

        validateOptions(ruleDefinition, options);

        RuleOption actionOption = options.getFirstOption(resolveAliases(ChainActionOptions)).value();

        vector<RuleOption> remainingOptions = options.withoutOption(actionOption).getOptions();

        Rule rule = Rule(
            0,
            actionOption.getName(),
            actionOption.getValue(0).value(),
            canonicalizeValues(remainingOptions),
            counters
        );

        return applyRuleExpansions(rule);
    }

    void RuleParser::validateOptions(const string &ruleDefinition, const PlainRuleOptionCollection& options)
    {
        vector<RuleOption> actions = options.getOptions(resolveAliases(ChainActionOptions));

        if (actions.size() != 1) {
            throw InvalidActionException(
                "Rule does not contain exactly one chain action (e.g. -I INPUT): " + ruleDefinition
            );
        }

        for (const string& nonRepeatableOption : NonRepeatableOptions) {
            vector<RuleOption> foundOptions = options.getOptions(resolveAliases({nonRepeatableOption}));
            if (foundOptions.size() > 1) {
                throw InvalidDefinitionException(format(
                    "Option {} may only be provided once for every definition. Rule: {}",
                    nonRepeatableOption,
                    ruleDefinition
                ));
            }
        }

        for (const RuleOption& singleValueOption : options.getOptions(resolveAliases(SingleValueOptions))) {
            if (singleValueOption.getValues().size() != 1) {
                throw InvalidDefinitionException(
                    "Option " + singleValueOption.getName() + " must have exactly one value. Rule: " + ruleDefinition
                );
            }
        }
    }

    string RuleParser::getOptionName(const string &token)
    {
        if (token.length() < 2 || token[0] != '-') {
            throw UnexpectedTokenException("Could not determine option name for token: " + token);
        }
        return token.substr(token[1] == '-' ? 2 : 1);
    }

    vector<Rule> RuleParser::applyRuleExpansions(Rule rule)
    {
        vector<Rule> rules;

        // '--match [tcp|udp]' is often implicit; explicitly add them if necessary to properly match

        if (rule.hasOption(RuleOption("p", {"tcp"})) || rule.hasOption(TcpSpecificMatchOptions)) {
            RuleOption matchOption = RuleOption("m", {"tcp"});
            if (!rule.hasOption(matchOption)) {
                rule = rule.withOptionBefore(matchOption, TcpMatchOptions);
            }
        }

        if (rule.hasOption(RuleOption("p", {"udp"}))) {
            RuleOption matchOption = RuleOption("m", {"udp"});
            if (!rule.hasOption(matchOption)) {
                rule = rule.withOptionBefore(matchOption, UdpMatchOptions);
            }
        }

        for (const string& expandingOption : ExpandingOptions) {
            optional<RuleOption> option = rule.getFirstOption(resolveAliases({expandingOption}));

            if (!option.has_value()) {
                continue;
            }

            optional<string> optionValue = option->getValue(0);

            if (!optionValue.has_value() || optionValue->find(',') == string::npos) {
                continue;
            }

            for (const string& item : util::string::split(optionValue.value(), ',')) {
                rules.push_back(
                    rule.withReplacedOption(
                        option->getName(),
                        RuleOption(expandingOption, {item}, option->isNegated())
                    )
                );
            }
        }

        if (rules.empty()) {
            rules.push_back(rule);
        }

        return rules;
    }

    vector<string> RuleParser::resolveAliases(const vector<string>& optionNames)
    {
        vector<string> resolved = optionNames;

        for (const string& optionName : optionNames) {
            if (OptionAliases.count(optionName) < 1) {
                continue;
            }
            for (const string& alias : OptionAliases[optionName]) {
                resolved.push_back(alias);
            }
        }

        return resolved;
    }

    vector<RuleOption> RuleParser::canonicalizeValues(const vector<RuleOption>& ruleOptions)
    {
        return util::set::selectAs<RuleOption, RuleOption>(ruleOptions, [](const RuleOption& ruleOption) {
            vector<string> sourceDestinationAliases = resolveAliases({"s", "d"});

            if (!util::set::has(sourceDestinationAliases, ruleOption.getName())) {
                return ruleOption;
            }

            vector<string> canonicalValues = util::set::selectAs<string, string>(
                ruleOption.getValues(),
                [](const string& value) { return canonicalizeLocation(value); }
            );

            return RuleOption(ruleOption.getName(), canonicalValues, ruleOption.isNegated());
        });
    }

    string RuleParser::canonicalizeLocation(const string& value)
    {
        string possibleIpAddress = value;
        string prefixLength;

        if (possibleIpAddress.find('/') != string::npos) {
            vector<string> parts = util::string::split(possibleIpAddress, '/');
            if (parts.size() > 2) {
                return value;
            }

            if (!util::string::isNumeric(parts[1])) {
                return value;
            }

            possibleIpAddress = parts[0];
            prefixLength = parts[1];
        }

        IPAddress ipAddress;
        if (!IPAddress::tryParse(possibleIpAddress, ipAddress)) {
            return value;
        }

        if (prefixLength.empty()) {
            prefixLength = ipAddress.family() == AddressFamily::IPv4 ? "32" : "128";
        }

        return ipAddress.toString() + "/" + prefixLength;
    }
}
