#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <unordered_map>
#include <vector>

#include "structures.h"

namespace iptmerge::netfilter::detail
{
    using namespace iptmerge::netfilter::structure;
    using namespace std;

    inline namespace exports
    {
        class RuleParser
        {
        private:
            static void validateOptions(const string& ruleDefinition, const PlainRuleOptionCollection& options);

            [[nodiscard]] static string getOptionName(const string& token);
            [[nodiscard]] static vector<Rule> applyRuleExpansions(Rule rule);
            [[nodiscard]] static vector<string> resolveAliases(const vector<string>& optionNames);
            [[nodiscard]] static vector<RuleOption> canonicalizeValues(const vector<RuleOption>& ruleOptions);
            [[nodiscard]] static string canonicalizeLocation(const string& value);

        public:
            [[nodiscard]] static vector<Rule> parse(const string& ruleDefinition);
        };
    }
}

