#include "rule_tokenizer.h"

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include "../exceptions.h"
#include "../util.h"

namespace iptmerge::netfilter::detail
{
    using namespace iptmerge;
    using namespace std;

    const string RuleTokenizer::NegationToken = "!";

    vector<string> RuleTokenizer::tokenize(const string &definition)
    {
        vector<string> tokens;
        ulong position = 0;

        while (position < definition.length()) {
            ulong foundLength;
            string chunk = definition.substr(position);

            foundLength = getLengthForWhitespace(chunk);
            if (foundLength > 0) {
                position += foundLength;
                continue;
            }

            foundLength = getLengthForQuotedString(chunk);
            if (foundLength > 0) {
                tokens.push_back(util::string::unescape(chunk.substr(0, foundLength)));
                position += foundLength;
                continue;
            }

            foundLength = getLengthForLongOptionWithAssignment(chunk);
            if (foundLength > 0) {
                // Length - 1 to strip off '='
                tokens.push_back(chunk.substr(0, foundLength - 1));
                position += foundLength;
                continue;
            }

            foundLength = getLengthForShortOption(chunk);
            if (foundLength > 0) {
                tokens.push_back(chunk.substr(0, foundLength));
                position += foundLength;
                continue;
            }

            foundLength = getLengthUntilDelimiter(chunk);
            if (foundLength > 0) {
                tokens.push_back(chunk.substr(0, foundLength));
                position += foundLength;
                continue;
            }

            throw UnexpectedTokenException("Failed to tokenize chunk: " + chunk);
        }

        return tokens;
    }

    vector<vector<string>> RuleTokenizer::groupTokens(const vector<string>& tokens)
    {
        vector<vector<string>> tokenGroups;
        vector<string> currentGroup;

        for (string const& token : tokens) {
            bool newGroup = false;

            if (token.length() == 0) {
                continue;
            }

            // Group always starts with a token starting with a '-' unless the group is currently only a negation token
            if (token[0] == '-' && (currentGroup.size() != 1 || currentGroup[0] != NegationToken)) {
                newGroup = true;
            }

            if (token == NegationToken) {
                newGroup = true;
            }

            if (newGroup && !currentGroup.empty()) {
                tokenGroups.push_back(currentGroup);
                currentGroup = vector<string>();
            }

            currentGroup.push_back(token);
        }

        if (!currentGroup.empty()) {
            tokenGroups.push_back(currentGroup);
        }

        return tokenGroups;
    }

    ulong RuleTokenizer::getLengthForWhitespace(const string &chunk)
    {
        for (ulong i = 0; i < chunk.length(); i++) {
            if (!isspace(chunk[i])) {
                return i;
            }
        }
        return 0;
    }

    ulong RuleTokenizer::getLengthForQuotedString(const string &chunk)
    {
        if (chunk.length() < 2) {
            return 0;
        }

        if (chunk[0] != '"' && chunk[0] != '\'') {
            return 0;
        }

        char quoteChar = chunk[0];
        bool isEscaping = false;

        for (ulong i = 1; i < chunk.length(); i++) {
            if (isEscaping) {
                isEscaping = false;
                continue;
            }

            if (chunk[i] == '\\') {
                isEscaping = true;
                continue;
            }

            if (chunk[i] == quoteChar) {
                return i + 1;
            }
        }

        throw UnclosedStringException("Could not find closing quote for string in chunk: " + chunk);
    }

    ulong RuleTokenizer::getLengthForShortOption(const string &chunk)
    {
        if (chunk.length() < 2 || chunk[0] != '-' || chunk[1] == '-' || isDelimiter(chunk[1])) {
            return 0;
        }

        return 2;
    }

    ulong RuleTokenizer::getLengthForLongOptionWithAssignment(const string &chunk)
    {
        if (chunk.length() < 3 || chunk[0] != '-' || chunk[1] != '-' || isDelimiter(chunk[2])) {
            return 0;
        }

        for (ulong i = 3; i < chunk.length() - 1; i++) {
            if (isDelimiter(chunk[i]) && chunk[i + 1] == '=') {
                return i + 2;
            }
        }

        return 0;
    }

    ulong RuleTokenizer::getLengthUntilDelimiter(const string &chunk)
    {
        for (ulong i = 0; i < chunk.length(); i++) {
            if (isDelimiter(chunk[i])) {
                return i;
            }
        }
        return chunk.length();
    }

    bool RuleTokenizer::isDelimiter(char character)
    {
        return character == '"' || character == '\'' || isspace(character);
    }
}
