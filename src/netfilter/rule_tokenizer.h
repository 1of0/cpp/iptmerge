#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <string>
#include <vector>

namespace iptmerge::netfilter::detail
{
    using namespace std;

    inline namespace exports
    {
        class RuleTokenizer
        {
        private:
            [[nodiscard]] static ulong getLengthForWhitespace(const string& chunk);
            [[nodiscard]] static ulong getLengthForQuotedString(const string& chunk);
            [[nodiscard]] static ulong getLengthForShortOption(const string& chunk);
            [[nodiscard]] static ulong getLengthForLongOptionWithAssignment(const string& chunk);
            [[nodiscard]] static ulong getLengthUntilDelimiter(const string& chunk);
            [[nodiscard]] static bool isDelimiter(char character);

        public:
            static const string NegationToken;

            [[nodiscard]] static vector<string> tokenize(const string& definition);

            [[nodiscard]] static vector<vector<string>> groupTokens(const vector<string>& tokens);
        };
    }
}

