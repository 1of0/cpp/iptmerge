#include "state_parser.h"

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <fstream>
#include <regex>
#include "rule_parser.h"
#include "../exceptions.h"
#include "../util.h"

namespace iptmerge::netfilter::detail
{
    using namespace iptmerge::netfilter::structure;
    using namespace iptmerge;

    regex PatternChain(R"(^:(.*?)\s+(.*?)\s+(\[\d+:\d+\]))");

    State StateParser::parseStream(istream& stream)
    {
        string currentTableName;
        vector<Chain> tableChains;
        vector<Rule> tableRules;
        vector<Table> tables;

        string line;
        long ruleIndex = 0;

        while (getline(stream, line)) {
            line = util::string::trim(line);

            if (line.empty() || line[0] == '#') {
                continue;
            }

            if (line[0] == '*') {
                currentTableName = line.substr(1);
                continue;
            }

            if (currentTableName.empty()) {
                throw InvalidDefinitionException("Found definition outside table scope");
            }

            smatch chainMatches;
            if (regex_search(line, chainMatches, PatternChain)) {
                tableChains.emplace_back(chainMatches[1], chainMatches[2], chainMatches[3]);
                continue;
            }

            if (line == "COMMIT") {
                tables.emplace_back(currentTableName, tableChains, tableRules);
                currentTableName.clear();
                tableChains.clear();
                tableRules.clear();
                ruleIndex = 0;
                continue;
            }

            for (const Rule& rule : RuleParser::parse(line)) {
                tableRules.push_back(rule.withIndex(ruleIndex++));
            }
        }

        return State(tables);
    }

    State StateParser::parseString(const string& stateDefinition)
    {
        istringstream stream(stateDefinition);
        return parseStream(stream);
    }

    State StateParser::parseFile(const string& stateFile)
    {
        ifstream stream;
        stream.open(stateFile);
        State state = parseStream(stream);
        stream.close();
        return state;
    }
}
