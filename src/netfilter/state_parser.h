#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <iostream>
#include <unordered_map>
#include <vector>

#include "structures.h"

namespace iptmerge::netfilter::detail
{
    using namespace iptmerge::netfilter::structure;
    using namespace std;

    inline namespace exports
    {
        class StateParser
        {
        public:
            [[nodiscard]] static State parseStream(istream& stream);

            [[nodiscard]] static State parseString(const string& stateDefinition);

            [[nodiscard]] static State parseFile(const string& stateFile);
        };
    }
}

