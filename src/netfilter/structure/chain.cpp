#include "chain.h"

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <fmt/format.h>

namespace iptmerge::netfilter::structure::detail
{
    using namespace fmt;
    using namespace std;

    Chain::Chain(string name, string policy, string counters)
        : name(move(name)), policy(move(policy)), counters(move(counters)) {}

    string Chain::getName() const
    {
        return name;
    }

    string Chain::getPolicy() const
    {
        return policy;
    }

    string Chain::getCounters() const
    {
        return counters;
    }

    Chain Chain::withCounters(const string &newCounters) const
    {
        return Chain(name, policy, newCounters);
    }

    string Chain::toString(bool canonical) const
    {
        return canonical ? name : format(":{} {} {}", name, policy, counters);
    }

    bool Chain::equals(const Chain &other) const
    {
        return name == other.name;
    }

    bool Chain::fullyEquals(const Chain &other) const
    {
        return toString() == other.toString();
    }
}
