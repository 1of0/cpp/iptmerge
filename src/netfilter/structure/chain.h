#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <string>

#include "object.h"
#include "chain.h"

namespace iptmerge::netfilter::structure::detail
{
    using namespace std;

    inline namespace exports
    {
        class Chain : public FullyEquatableObject<Chain>
        {
        private:
            string name;
            string policy;
            string counters;

        public:
            Chain(string name, string policy, string counters);

            [[nodiscard]] string getName() const;
            [[nodiscard]] string getPolicy() const;
            [[nodiscard]] string getCounters() const;
            [[nodiscard]] Chain withCounters(const string& newCounters) const;

            [[nodiscard]] string toString() const override { return toString(false); }
            [[nodiscard]] string toString(bool canonical) const override;

            [[nodiscard]] bool equals(const Chain& other) const override;
            [[nodiscard]] bool fullyEquals(const Chain& other) const override;
        };
    }
}
