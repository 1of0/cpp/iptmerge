#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <string>

namespace iptmerge::netfilter::structure::detail
{
    using namespace std;

    inline namespace exports
    {
        class Object
        {
        public:
            [[nodiscard]] virtual string toString() const
            {
                return toString(false);
            }

            [[nodiscard]] virtual string toString(bool canonical) const
            {
                return "[object]";
            }

            [[nodiscard]] virtual bool operator<(const Object& other) const
            {
                return toString(true) < other.toString(true);
            }

            [[nodiscard]] virtual bool operator>(const Object& other) const
            {
                return toString(true) > other.toString(true);
            }
        };

        template<typename T>
        class FullyEquatableObject : public Object
        {
        public:
            [[nodiscard]] virtual bool equals(const T& other) const = 0;

            [[nodiscard]] virtual bool fullyEquals(const T& other) const
            {
                return equals(other);
            }

            [[nodiscard]] virtual bool operator==(const T& other) const
            {
                return equals(other);
            }

            [[nodiscard]] virtual bool operator!=(const T& other) const
            {
                return !equals(other);
            }
        };
    }
}
