#include "rule.h"

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include "../../compile_time.h"
#include "../../util.h"

namespace iptmerge::netfilter::structure::detail
{
    using namespace fmt;
    using namespace std;

    Rule::Rule(long index, string action, string chain, vector<RuleOption> items, string counters) :
        index(index),
        action(std::move(action)),
        chain(std::move(chain)),
        RuleOptionCollection(std::move(items)),
        counters(std::move(counters)) {}

    long Rule::getIndex() const
    {
        return index;
    }

    const string& Rule::getAction() const
    {
        return action;
    }

    const string& Rule::getChain() const
    {
        return chain;
    }

    const string& Rule::getCounters() const
    {
        return counters;
    }

    Rule Rule::withIndex(long newIndex) const
    {
        return Rule(newIndex, action, chain, items, counters);
    }

    Rule Rule::withAction(const string& newAction) const
    {
        return Rule(index, newAction, chain, items, counters);
    }

    Rule Rule::withCounters(const string& newCounters) const
    {
        return Rule(index, action, chain, items, newCounters);
    }

    string Rule::toString(bool canonical) const
    {
        string buffer;

        if (!canonical && !counters.empty() && action != "D") {
            buffer += counters + " ";
        }

        string actionCopy = action;

        if (canonical && action == "I") {
            actionCopy = "A";
        }

        buffer += actionCopy.length() > 1
            ? "--" + actionCopy
            : "-" + actionCopy;

        buffer += " " + chain;

        if (!items.empty()) {
            buffer += format(" {}", join(canonical ? util::set::sorted(items) : items, " "));
        }

        return buffer;
    }

    Rule Rule::withReplacedOption(const string& optionName, const RuleOption& replacement) const
    {
        return Rule(index, action, chain, collectionWithReplacedOption(optionName, replacement), counters);
    }

    Rule Rule::withoutOption(const RuleOption& option) const
    {
        return Rule(index, action, chain, collectionWithoutOption(option), counters);
    }

    Rule Rule::withOptionBefore(const RuleOption& option, const vector<string>& optionNames) const
    {
        vector<RuleOption> itemsCopy = items;

        for (auto itemsIterator = itemsCopy.begin(); itemsIterator != itemsCopy.end(); ++itemsIterator) {
            if (util::set::has(optionNames, itemsIterator->getName())) {
                itemsCopy.insert(itemsIterator, option);
                return Rule(index, action, chain, itemsCopy, counters);
            }
        }

        itemsCopy.push_back(option);
        return Rule(index, action, chain, itemsCopy, counters);
    }

    bool Rule::equals(const Rule& other) const
    {
        return other.chain == chain
            && areActionsCompatible(other.action, action)
            && util::set::areEqual(other.items, items);
    }

    bool Rule::fullyEquals(const Rule &other) const
    {
        return other.chain == chain
            && other.action == action
            && other.counters == counters
            && util::set::areEqual(other.items, items);
    }

    bool Rule::areActionsCompatible(const string& a, const string& b)
    {
        return (a == "I" || a == "A") && (b == "I" || b == "A");
    }
}
