#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <optional>
#include <string>
#include <vector>

#include "rule_option.h"
#include "rule_option_collection.h"

namespace iptmerge::netfilter::structure::detail
{
    using namespace std;

    inline namespace exports
    {
        class Rule : public RuleOptionCollection, public FullyEquatableObject<Rule>
        {
        private:
            ulong index;
            string action;
            string chain;
            string counters;

            [[nodiscard]] static bool areActionsCompatible(const string& a, const string& b);

        public:
            Rule(long index, string action, string chain, vector<RuleOption> items, string counters = "");

            [[nodiscard]] long getIndex() const;
            [[nodiscard]] const string& getAction() const;
            [[nodiscard]] const string& getChain() const;
            [[nodiscard]] const string& getCounters() const;

            [[nodiscard]] Rule withIndex(long index) const;
            [[nodiscard]] Rule withAction(const string& action) const;
            [[nodiscard]] Rule withCounters(const string& counters) const;

            [[nodiscard]] Rule withReplacedOption(const string& optionName, const RuleOption& replacement) const;
            [[nodiscard]] Rule withoutOption(const RuleOption& option) const;
            [[nodiscard]] Rule withOptionBefore(const RuleOption& option, const vector<string>& optionNames) const;

            [[nodiscard]] string toString() const override { return toString(false); }
            [[nodiscard]] string toString(bool canonical) const override;

            [[nodiscard]] bool equals(const Rule& other) const override;
            [[nodiscard]] bool fullyEquals(const Rule& other) const override;
        };
    }
}
