#include "rule_option.h"

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include "../../util.h"

namespace iptmerge::netfilter::structure::detail
{
    using namespace std;

    RuleOption::RuleOption(string name, vector<string> values, bool negated)
        : name(move(name)), values(move(values)), negated(negated) {}

    string RuleOption::getName() const
    {
        return name;
    }

    vector<string> RuleOption::getValues() const
    {
        return values;
    }

    bool RuleOption::isNegated() const {
        return negated;
    }

    optional<string> RuleOption::getValue(int index) const
    {
        if (index >= values.size()) {
            return nullopt;
        }
        return values[index];
    }

    string RuleOption::toString(bool canonical) const
    {
        string buffer = negated ? "! " : "";

        buffer += (name.length() > 1)
            ? "--" + name
            : "-" + name;

        for (const string& value : values) {
            buffer += " " + (util::string::requiresEscaping(value) ? util::string::escape(value) : value);
        }

        return buffer;
    }

    bool RuleOption::equals(const RuleOption& other) const
    {
        return name == other.name
            && negated == other.negated
            && util::set::areEqual(values, other.values);
    }
}
