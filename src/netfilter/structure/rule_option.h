#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <string>
#include <vector>
#include <optional>

#include "object.h"

namespace iptmerge::netfilter::structure::detail
{
    using namespace std;

    inline namespace exports
    {
        class RuleOption : public FullyEquatableObject<RuleOption>
        {
        private:
            string name;
            vector<string> values;
            bool negated;

        public:
            explicit RuleOption(string name, vector<string> values = {}, bool negated = false);

            [[nodiscard]] string getName() const;
            [[nodiscard]] vector<string> getValues() const;
            [[nodiscard]] optional<string> getValue(int index) const;
            [[nodiscard]] bool isNegated() const;

            [[nodiscard]] string toString() const override { return toString(false); }
            [[nodiscard]] string toString(bool canonical) const override;

            [[nodiscard]] bool equals(const RuleOption& other) const override;
        };
    }
}
