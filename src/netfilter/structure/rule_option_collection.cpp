#include "rule_option_collection.h"

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <optional>
#include "../../util.h"

namespace iptmerge::netfilter::structure::detail
{
    using namespace std;

    RuleOptionCollection::RuleOptionCollection(vector<RuleOption> items) : items(std::move(items)) {}

    vector<RuleOption> RuleOptionCollection::collectionWithReplacedOption(
        const string& optionName,
        const RuleOption& replacement
    ) const {
        return util::set::selectAs<RuleOption, RuleOption>(
            items,
            [&optionName, &replacement](const RuleOption& item) {
                return item.getName() == optionName ? replacement : item;
            }
        );
    }

    vector<RuleOption> RuleOptionCollection::collectionWithoutOption(const RuleOption& option) const
    {
        return util::set::where<RuleOption>(items, [&option](const RuleOption& item) {
            return item != option;
        });
    }

    vector<RuleOption> RuleOptionCollection::getOptions() const {
        return items;
    }

    vector<RuleOption> RuleOptionCollection::getOptions(const vector<string>& names) const
    {
        return util::set::where<RuleOption>(items, [&names](const RuleOption& item) {
            return util::set::has(names, item.getName());
        });
    }

    optional<RuleOption> RuleOptionCollection::getFirstOption(const vector<string> &names) const
    {
        vector<RuleOption> itemsByName = getOptions(names);
        return itemsByName.empty()
            ? nullopt
            : optional<RuleOption>(itemsByName.front());
    }

    bool RuleOptionCollection::hasOption(const RuleOption& search) const
    {
        for (const RuleOption& option : items) {
            if (option == search) {
                return true;
            }
        }
        return false;
    }

    bool RuleOptionCollection::hasOption(const vector<string> &names) const
    {
        for (const RuleOption& option : items) {
            if (util::set::has(names, option.getName())) {
                return true;
            }
        }
        return false;
    }

    PlainRuleOptionCollection::PlainRuleOptionCollection(vector<RuleOption> items)
        : RuleOptionCollection(std::move(items)) {}

    PlainRuleOptionCollection PlainRuleOptionCollection::withReplacedOption(
        const string& option_name,
        const RuleOption& replacement
    ) const {
        return PlainRuleOptionCollection(collectionWithReplacedOption(option_name, replacement));
    }

    PlainRuleOptionCollection PlainRuleOptionCollection::withoutOption(const RuleOption& option) const
    {
        return PlainRuleOptionCollection(collectionWithoutOption(option));
    }
}
