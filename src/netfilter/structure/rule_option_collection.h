#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <optional>
#include <string>

#include "object.h"
#include "rule_option.h"

namespace iptmerge::netfilter::structure::detail
{
    using std::optional;
    using std::string;
    using std::vector;

    inline namespace exports
    {
        class RuleOptionCollection
        {
        protected:
            vector<RuleOption> items;

            explicit RuleOptionCollection(vector<RuleOption> items);

            [[nodiscard]] vector<RuleOption> collectionWithReplacedOption(
                const string& optionName,
                const RuleOption& replacement
            ) const;

            [[nodiscard]] vector<RuleOption> collectionWithoutOption(const RuleOption& option) const;

        public:
            [[nodiscard]] vector<RuleOption> getOptions() const;
            [[nodiscard]] vector<RuleOption> getOptions(const vector<string>& names) const;
            [[nodiscard]] optional<RuleOption> getFirstOption(const vector<string>& names) const;
            [[nodiscard]] bool hasOption(const RuleOption& search) const;
            [[nodiscard]] bool hasOption(const vector<string> &names) const;
        };

        class PlainRuleOptionCollection : public RuleOptionCollection
        {
        public:
            explicit PlainRuleOptionCollection(vector<RuleOption> items);

            [[nodiscard]] PlainRuleOptionCollection withReplacedOption(
                const string& option_name,
                const RuleOption& replacement
            ) const;
            [[nodiscard]] PlainRuleOptionCollection withoutOption(const RuleOption& option) const;
        };
    }
}
