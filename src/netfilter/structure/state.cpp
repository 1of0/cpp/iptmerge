#include "state.h"

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include "../../util.h"

namespace iptmerge::netfilter::structure::detail
{
    using namespace iptmerge;
    using namespace std;

    State::State(vector<Table> tables)
        : tables(std::move(tables)) {}

    string State::toString(bool canonical) const
    {
        string buffer;
        for (const Table& table : canonical ? util::set::sorted(tables) : tables) {
            buffer += table.toString(canonical);
        }
        return buffer;
    }

    vector<Table> State::getTables() const
    {
        return tables;
    }

    unordered_map<string, Table> State::getTablesIndexedByName() const
    {
        unordered_map<string, Table> mappedTables;
        for (const Table& table : tables) {
            mappedTables.emplace(table.getName(), table);
        }
        return mappedTables;
    }

    bool State::equals(const State& other) const
    {
        return util::set::areEqual(other.tables, tables);
    }

    bool State::fullyEquals(const State& other) const
    {
        return util::set::areFullyEqual(other.tables, tables);
    }
}
