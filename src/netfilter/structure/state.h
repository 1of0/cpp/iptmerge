#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <string>
#include <unordered_map>

#include "table.h"

namespace iptmerge::netfilter::structure::detail
{
    using namespace std;

    inline namespace exports
    {
        class State : public FullyEquatableObject<State>
        {
        private:
            vector<Table> tables;

        public:
            explicit State(vector<Table> tables);

            [[nodiscard]] vector<Table> getTables() const;

            [[nodiscard]] unordered_map<string, Table> getTablesIndexedByName() const;

            [[nodiscard]] string toString() const override { return toString(false); }
            [[nodiscard]] string toString(bool canonical) const override;

            [[nodiscard]] bool equals(const State& other) const override;
            [[nodiscard]] bool fullyEquals(const State& other) const override;
        };
    }
}
