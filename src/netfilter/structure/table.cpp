#include "table.h"

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include "../../util.h"

namespace iptmerge::netfilter::structure::detail
{
    using namespace iptmerge;
    using namespace std;

    Table::Table(string name, vector<Chain> chains, vector<Rule> rules)
        : name(std::move(name)), chains(std::move(chains)), rules(std::move(rules)) {}

    string Table::getName() const
    {
        return name;
    }

    vector<Chain> Table::getChains() const
    {
        return chains;
    }

    vector<Rule> Table::getRules() const
    {
        return rules;
    }

    unordered_map<string, Chain> Table::getChainsIndexedByName() const
    {
        unordered_map<string, Chain> mappedChains;
        for (const Chain& chain : chains) {
            mappedChains.emplace(chain.getName(), chain);
        }
        return mappedChains;
    }

    string Table::toString(bool canonical) const
    {
        string buffer = "*" + name + "\n";

        for (const Chain& chain : canonical ? util::set::sorted(chains) : chains) {
            buffer += chain.toString(canonical) + "\n";
        }

        for (const Rule& rule : canonical ? util::set::sorted(rules) : rules) {
            buffer += rule.toString(canonical) + "\n";
        }

        return buffer += "COMMIT\n";
    }

    bool Table::equals(const Table& other) const
    {
        return other.name == name;
    }

    bool Table::fullyEquals(const Table& other) const
    {
        return other.name == name
            && util::set::areFullyEqual(other.chains, chains)
            && util::set::areFullyEqual(other.rules, rules);
    }
}
