#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <string>
#include <unordered_map>

#include "chain.h"
#include "rule.h"

namespace iptmerge::netfilter::structure::detail
{
    using namespace std;

    inline namespace exports
    {
        class Table : public FullyEquatableObject<Table>
        {
        private:
            string name;
            vector<Chain> chains;
            vector<Rule> rules;

        public:
            explicit Table(string name, vector<Chain> chains = vector<Chain>(), vector<Rule> rules = vector<Rule>());

            [[nodiscard]] string getName() const;
            [[nodiscard]] vector<Chain> getChains() const;
            [[nodiscard]] vector<Rule> getRules() const;

            [[nodiscard]] unordered_map<string, Chain> getChainsIndexedByName() const;

            [[nodiscard]] string toString() const override { return toString(false); }
            [[nodiscard]] string toString(bool canonical) const override;

            [[nodiscard]] bool equals(const Table& other) const override;
            [[nodiscard]] bool fullyEquals(const Table& other) const override;
        };
    }
}
