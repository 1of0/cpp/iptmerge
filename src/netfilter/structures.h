#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include "structure/object.h"
#include "structure/chain.h"
#include "structure/rule_option.h"
#include "structure/rule_option_collection.h"
#include "structure/rule.h"
#include "structure/table.h"
#include "structure/state.h"

namespace iptmerge::netfilter::structure
{
    using namespace detail::exports;
}
