#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <functional>
#include <optional>
#include <sstream>
#include <string>
#include <vector>

namespace iptmerge::util
{
    namespace string::detail
    {
        using namespace std;

        inline namespace exports
        {
            inline vector<std::string> split(const std::string& input, char separator)
            {
                istringstream tokenStream(input);
                vector<std::string> items;
                std::string item;

                while (getline(tokenStream, item, separator)) {
                    items.push_back(item);
                }

                return items;
            }

            inline bool isNumeric(const std::string& input)
            {
                for (ulong i = 0; i < input.length(); i++) {
                    if (!isdigit(input[i])) {
                        return false;
                    }
                }
                return true;
            }

            inline bool isCharacterEscapable(char character)
            {
                return character == '"' || character == '\'' || character == '\\';
            }

            inline bool requiresEscaping(const std::string& input)
            {
                for (const char& c : input) {
                    if (!isalnum(c) && c != ':' && c != '_' && c != '-') {
                        return true;
                    }
                }
                return false;
            }

            inline std::string escape(const std::string& input)
            {
                std::string escaped;
                for (char character : input) {
                    if (isCharacterEscapable(character)) {
                        escaped += '\\';
                    }
                    escaped += character;
                }
                return "\"" + escaped + "\"";
            }

            inline std::string unescape(const std::string& input)
            {
                std::string inputWork = input;

                if (input.length() > 1) {
                    if (input[0] == '"' && input[input.length() - 1] == '"' ||
                        input[0] == '\'' && input[input.length() - 1] == '\'') {
                        inputWork = input.substr(1, input.length() - 2);
                    }
                }

                std::string unescaped;
                for (ulong i = 0; i < inputWork.length(); i++) {
                    if (inputWork[i] == '\\' && isCharacterEscapable(inputWork[i + 1])) {
                        unescaped += inputWork[++i];
                        continue;
                    }
                    unescaped += inputWork[i];
                }
                return unescaped;
            }

            inline std::string trimLeft(const std::string& input)
            {
                for (ulong i = 0; i < input.length(); i++) {
                    if (!isspace(input[i])) {
                        return input.substr(i);
                    }
                }
                return input;
            }

            inline std::string trimRight(const std::string& input)
            {
                for (ulong i = input.length() - 1; i >= 0; i--) {
                    if (!isspace(input[i])) {
                        return input.substr(0, i + 1);
                    }
                }
                return input;
            }

            inline std::string trim(const std::string& input)
            {
                for (ulong i = 0; i < input.length(); i++) {
                    if (isspace(input[i])) {
                        continue;
                    }

                    for (ulong j = input.length() - 1; j >= i; j--) {
                        if (!isspace(input[j])) {
                            return input.substr(i, j - i + 1);
                        }
                    }
                }
                return input;
            }
        }
    }

    namespace string
    {
        using namespace detail::exports;
    }

    namespace set::detail
    {
        using namespace std;

        inline namespace exports
        {
            template<typename T>
            inline vector<T> diffOf(vector<T> a, vector<T> b)
            {
                vector<T> diff;
                sort(a.begin(), a.end());
                sort(b.begin(), b.end());

                set_difference(
                    a.begin(), a.end(),
                    b.begin(), b.end(),
                    back_inserter(diff)
                );
                return diff;
            }

            template<typename T>
            inline vector<T> unionOf(vector<T> a, vector<T> b)
            {
                vector<T> unionSet;
                sort(a.begin(), a.end());
                sort(b.begin(), b.end());

                set_union(a.begin(), a.end(), b.begin(), b.end(), back_inserter(unionSet));
                return unionSet;
            }

            template<typename T>
            inline vector<T> sorted(vector<T> set)
            {
                sort(set.begin(), set.end());
                return set;
            }

            template<typename T>
            inline vector<T> sortedBy(vector<T> set, function<bool(const T&, const T&)> compare)
            {
                sort(set.begin(), set.end(), compare);
                return set;
            }

            template<typename T>
            inline bool areEqual(vector<T> a, vector<T> b)
            {
                sort(a.begin(), a.end());
                sort(b.begin(), b.end());

                return equal(
                    a.begin(), a.end(),
                    b.begin(), b.end()
                );
            }

            template<typename T>
            inline bool areFullyEqual(vector<T> a, vector<T> b)
            {
                sort(a.begin(), a.end());
                sort(b.begin(), b.end());

                return equal(
                    a.begin(), a.end(),
                    b.begin(), b.end(),
                    [](const T& a, const T& b) { return a.fullyEquals(b); }
                );
            }

            template<typename T>
            inline bool has(const vector<T>& input, const T& search)
            {
                for (const T& item : input) {
                    if (item == search) {
                        return true;
                    }
                }
                return false;
            }

            template<typename T>
            inline vector<T> where(const vector<T>& input, function<bool(const T&)> predicate)
            {
                vector<T> output;
                output.reserve(input.size());
                for (const T& item : input) {
                    if (predicate(item)) {
                        output.push_back(item);
                    }
                }
                output.shrink_to_fit();
                return output;
            }

            template<typename SelectResult, typename T>
            inline vector<SelectResult> selectAs(const vector<T>& input, function<SelectResult(const T&)> selector)
            {
                vector<SelectResult> output;
                output.reserve(input.size());
                for (const T& item : input) {
                    output.push_back(selector(item));
                }
                return output;
            }
        }
    }

    namespace set
    {
        using namespace detail::exports;
    }

    namespace map::detail
    {
        using namespace std;

        inline namespace exports
        {
            template<typename K, typename V>
            inline vector<V> getValues(unordered_map<K, V> map)
            {
                vector<V> values;
                for (const pair<K, V>& kv : map) {
                    values.push_back(kv.second);
                }
                return values;
            }

            template<typename K, typename T>
            inline optional<K> getKeyOfVectorContainingValue(unordered_map<K, vector<T>> map, T value)
            {
                for (const pair<K, vector<T>>& kv : map) {
                    if (find(kv.second.begin(), kv.second.end(), value) != kv.second.end()) {
                        return kv.first;
                    }
                }
                return nullopt;
            }
        }
    }

    namespace map
    {
        using namespace detail::exports;
    }
}
