#pragma once

/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <libgen.h>
#include <limits.h>
#include <string>
#include <unistd.h>

using namespace std;

inline string readLinkToString(const string& path)
{
    char buffer[PATH_MAX];
    ssize_t pathLength = readlink(path.c_str(), buffer, PATH_MAX - 1);
    return pathLength < 0 ? "" : string(buffer, pathLength);
}

inline string getExecutablePath()
{
    for (const string& procReference : {
        "/proc/self/exe",
        "/proc/curproc/file",
        "/proc/self/path/a.out"
    }) {
        string followedLink = readLinkToString(procReference);
        if (!followedLink.empty()) {
            return followedLink;
        }
    }
    return "";
}

inline string getExecutableDir()
{
    string executablePath = getExecutablePath();
    if (executablePath.empty()) {
        return "";
    }
    return string().assign(dirname(executablePath.data()));
}

inline string getTestProjectDir()
{
    return getExecutableDir() + "/../../tests";
}

inline string getTestStatePath(const string& state)
{
    return getTestProjectDir() + "/vectors/" + state + ".state";
}
