/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <catch2/catch.hpp>
#include "../iptmerge-tests.h"
#include "../../src/compile_time.h"

using namespace iptmerge::merge::configuration;
using namespace iptmerge::netfilter;
using namespace std;

TEST_CASE("MergeBehaviour", "[unit][configuration]")
{
    auto mergeBehaviour = YAML::LoadFile(getTestProjectDir() + "/vectors/merge-behaviour.yml").as<MergeBehaviour>();
    REQUIRE(mergeBehaviour.unmanaged.chainStrategy == StrategyKind::Whitelist);
    REQUIRE(mergeBehaviour.unmanaged.ruleStrategy == StrategyKind::Whitelist);
    REQUIRE(mergeBehaviour.unmanaged.ruleMatchAgainst == MatchKind::Comment);
    REQUIRE(regex_search("foo-bar", mergeBehaviour.unmanaged.chainWhitelists[0]));
    REQUIRE(regex_search("foo", mergeBehaviour.unmanaged.chainWhitelists[0]));
    REQUIRE(!regex_search("bar", mergeBehaviour.unmanaged.chainWhitelists[0]));
    REQUIRE(regex_search("foo-bar", mergeBehaviour.unmanaged.ruleWhitelists[0]));
    REQUIRE(regex_search("foo", mergeBehaviour.unmanaged.ruleWhitelists[0]));
    REQUIRE(!regex_search("bar", mergeBehaviour.unmanaged.ruleWhitelists[0]));
    REQUIRE(!regex_search("foo", mergeBehaviour.managed.chainPattern));
    REQUIRE(regex_search("a-managed-foo", mergeBehaviour.managed.chainPattern));
    REQUIRE(!regex_search("b-managed-foo", mergeBehaviour.managed.chainPattern));
    REQUIRE(!regex_search("foo", mergeBehaviour.managed.rulePattern));
    REQUIRE(!regex_search("a-managed-foo", mergeBehaviour.managed.rulePattern));
    REQUIRE(regex_search("b-managed-foo", mergeBehaviour.managed.rulePattern));
}
