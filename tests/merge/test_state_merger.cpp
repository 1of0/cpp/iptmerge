/**
 * SPDX-License-Identifier: MIT
 * Find the full license text at: https://gitlab.com/1of0/cpp/iptmerge/-/blob/master/LICENSE.md
 */

#include <catch2/catch.hpp>
#include "../iptmerge-tests.h"
#include "../../src/compile_time.h"
#include "../../src/merge.h"
#include "../../src/netfilter.h"

using namespace iptmerge::merge::configuration;
using namespace iptmerge::merge;
using namespace iptmerge::netfilter;
using namespace std;

struct MergeTestData
{
    MergeBehaviour mergeBehaviour;
    string expectedStateFile;
    string generatedStateFile;
};

TEST_CASE("State merger", "[integration][merge]")
{
    auto parameters = GENERATE(values<MergeTestData>(
        {
            {
                MergeBehaviour
                {
                    {
                        regex("^a-managed"),
                        regex("^b-managed"),
                    },
                    {}
                },
                "merged.keep.keep"
            },
            {
                {
                    regex("^a-managed"),
                    regex("^b-managed"),
                },
                "merged.only-filter",
                "generated.only-filter"
            },
            {
                {
                    regex("^a-managed"),
                    regex("^b-managed"),
                },
                "merged.changed-policy",
                "generated.changed-policy"
            },
            {
                MergeBehaviour
                    {
                        {
                            regex("^a-managed"),
                            regex("^b-managed"),
                        },
                        {
                            StrategyKind::Purge,
                            StrategyKind::Purge,
                        }
                    },
                "merged.purge.purge"
            },
            {
                MergeBehaviour
                    {
                        {
                            regex("^a-managed"),
                            regex("^b-managed"),
                        },
                        {
                            StrategyKind::Whitelist,
                            StrategyKind::Keep,
                            MatchKind::Comment,
                            {regex("^foo")},
                        }
                    },
                "merged.whitelist.keep"
            },
            {
                MergeBehaviour
                    {
                        {
                            regex("^a-managed"),
                            regex("^b-managed"),
                        },
                        {
                            StrategyKind::Keep,
                            StrategyKind::Whitelist,
                            MatchKind::Comment,
                            {},
                            {regex("^foo")},
                        }
                    },
                "merged.keep.whitelist"
            },
            {
                MergeBehaviour
                    {
                        {
                            regex("^a-managed"),
                            regex("^b-managed"),
                        },
                        {
                            StrategyKind::Whitelist,
                            StrategyKind::Whitelist,
                            MatchKind::Comment,
                            {regex("^foo")},
                            {regex("^foo")},
                        }
                    },
                "merged.whitelist.whitelist"
            },
            {
                MergeBehaviour
                    {
                        {
                            regex("^a-managed"),
                            regex("^b-managed"),
                        },
                        {
                            StrategyKind::Keep,
                            StrategyKind::Whitelist,
                            MatchKind::Full,
                            {},
                            {regex("127\\.1\\.")},
                        }
                    },
                "merged.full-rule-match"
            },
        }
    ));

    vector<string> definedVectors = {
        "generated",
        "generated.only-filter",
        "generated.changed-policy",
        "merged.full-rule-match",
        "merged.keep.keep",
        "merged.keep.whitelist",
        "merged.only-filter",
        "merged.purge.purge",
        "merged.whitelist.keep",
        "merged.whitelist.whitelist",
        "running",
    };

    SECTION("Works with test vectors")
    {
        auto expectedState = StateParser::parseFile(getTestStatePath(parameters.expectedStateFile));
        auto runningState = StateParser::parseFile(getTestStatePath("running"));
        auto generatedState = parameters.generatedStateFile.empty()
            ? StateParser::parseFile(getTestStatePath("generated"))
            : StateParser::parseFile(getTestStatePath(parameters.generatedStateFile));

        INFO("Testing that configuration matches " + parameters.expectedStateFile);

        auto mergedState = StateMerger::merge(runningState, generatedState, parameters.mergeBehaviour);

        REQUIRE(mergedState == expectedState);
        REQUIRE(mergedState.fullyEquals(expectedState));
        REQUIRE(mergedState.toString() != expectedState.toString());
        REQUIRE(mergedState.toString(true) == expectedState.toString(true));

        for (const string& vector : definedVectors) {
            if (vector == parameters.expectedStateFile) {
                continue;
            }

            INFO("Testing that " + parameters.expectedStateFile + " does not match " + vector);

            auto expectedNotMatchingState = StateParser::parseFile(getTestStatePath(vector));

            REQUIRE(!mergedState.fullyEquals(expectedNotMatchingState));
            REQUIRE(mergedState.toString() != expectedNotMatchingState.toString());
        }
    }
}
